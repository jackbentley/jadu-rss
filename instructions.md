# Installation Instructions

1. Install Vagrant - https://www.vagrantup.com/
2. Add to your hosts file:
   ```
   192.168.99.99    rss.test
   ```
3. Run `vagrant up`
4. Point your browser to `http://rss.test`

## Running Tests

1. Login to the Vagrant machine
2. Navigate to the project directory `/var/app/rss/`
3. Run the test suites

```
vagrant ssh
cd /var/app/rss/
vendor/bin/phpunit
vendor/bin/behat
```
