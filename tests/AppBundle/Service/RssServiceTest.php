<?php

namespace Tests\AppBundle\Service;

use AppBundle\Service\RssService;
use PHPUnit\Framework\TestCase;

class RssServiceTest extends TestCase
{
    /**
     * @return RssService
     */
    protected function buildTestSubject()
    {
        return new RssService(false, null);
    }

    public function testConsumeFeed()
    {
        $service = $this->buildTestSubject();

        // Not ideal to use external sources, but the RSS parsing library is kinda limiting :(
        $feed = $service->consumeFeed('http://www.php.net/news.rss');

        $this->assertInstanceOf(\SimplePie::class, $feed);
    }
}
