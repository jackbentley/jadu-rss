1hr
 - Setting up basic Symfony 3 application + devops. Adding Vagrant, ansible and configuring everything to work together

1hr
 - Add front end framework, etc
 - Creating html-only app & design

1hr
 - Researching appropriate RSS parsing library and it's API.
 - Implement rss parsing library
 - Updating templates to work with new library
 - Add shiny images to feed items

1hr 30m
 - Initialize DB, create entity, migrations, etc
 - Update app to use DB data
 - Add data manipulation pages

1 hr
 - Adding testing suites
 - Writing tests
