<?php

namespace AppBundle\Service;

use Symfony\Component\Filesystem\Filesystem;

class RssService
{
    /**
     * @var bool
     */
    private $enableCache;

    /**
     * @var string
     */
    private $cacheDir;

    /**
     * RssService constructor.
     * @param bool $enableCache
     * @param string $cacheDir
     */
    public function __construct($enableCache, $cacheDir)
    {
        $this->enableCache = $enableCache;
        $this->cacheDir = $cacheDir;

        if ($this->enableCache) {
            $this->initCacheDir($this->cacheDir);
        }
    }

    /**
     * @param string $cacheDir
     */
    protected function initCacheDir($cacheDir)
    {
        if(!is_dir($cacheDir)) {
            $fileSystem = new Filesystem();
            $fileSystem->mkdir($cacheDir);
        }
    }

    /**
     * @param $feedUrl
     * @return \SimplePie
     */
    public function consumeFeed($feedUrl)
    {
        $feed = new \SimplePie();

        $feed->set_feed_url($feedUrl);
        $feed->enable_cache($this->enableCache);
        $feed->set_cache_location($this->cacheDir);

        $feed->init();

        return $feed;
    }
}
