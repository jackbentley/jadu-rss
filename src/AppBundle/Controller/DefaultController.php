<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Feed;
use AppBundle\Form\Type\DeleteType;
use AppBundle\Form\Type\FeedType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $rssService = $this->get('app_rss_service');
        $entityManager = $this->get('doctrine.orm.entity_manager');
        $repo = $entityManager->getRepository('AppBundle:Feed');

        $feeds = $repo->findAll();
        $columns = [];

        foreach ($feeds as $feed) {
            $columns[] = [
                'entity' => $feed,
                'feed' => $rssService->consumeFeed($feed->getLink())
            ];
        }

        return $this->render(
            '@App/default/index.html.twig',
            [
                'columns' => $columns
            ]
        );
    }

    /**
     * @param Request $request
     * @param Feed $feed
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editFeedAction(Request $request, Feed $feed)
    {
        $form = $this->createForm(FeedType::class, $feed);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($feed);
            $em->flush();

            return $this->redirectToRoute('rss_index');
        }

        return $this->render(
            '@App/rss/add.html.twig',
            [
                'entity' => $feed,
                'form' => $form->createView()
            ]
        );
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function addFeedAction(Request $request)
    {
        $feed = new Feed();

        return $this->editFeedAction($request, $feed);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteFeedAction(Request $request, Feed $feed)
    {
        $form = $this->createForm(DeleteType::class, $feed);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->remove($feed);
            $em->flush();

            return $this->redirectToRoute('rss_index');
        }

        return $this->render(
            '@App/rss/delete.html.twig',
            [
                'entity' => $feed,
                'form' => $form->createView()
            ]
        );
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function viewFeedAction(Request $request)
    {
        if (!$request->query->has('feed')) {
            return $this->redirectToRoute('rss_index');
        }

        $feedUrl = $request->query->get('feed');
        $rssService = $this->get('app_rss_service');

        return $this->render(
            '@App/rss/view.html.twig',
            [
                'feed' => $rssService->consumeFeed($feedUrl)
            ]
        );
    }
}
