Feature: Feeds
  In order to get a job at Jadu
  As an interviewee
  I need to write tests to ensure the feeds work as briefed

  Background:
    Given there are no feeds in the database

  Scenario: Adding a new RSS feed to the DB
    Given I am on the add feed page
    When I update the link to "http://www.php.net/news.rss"
    And I submit the feed
    Then I should see 1 feed columns

  Scenario: List all feeds within the DB
    Given There are 2 feeds
    When I view the homepage
    Then I should see 2 feed columns

  Scenario: Update individual feeds
    Given There are 1 feeds
    And I view the homepage
    When I edit the first feed
    And I update the link to "http://jack.rocks/thisRssFeedDoesntExist.xml"
    And I save the changes
    Then I should see 1 feed columns
    And The first feed should be empty

  Scenario: View a feed on it's own
    When I view the single view feed page with a valid feed
    Then I should see some items
