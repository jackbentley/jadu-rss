<?php

use Behat\Behat\Context\Context;
use Behat\MinkExtension\Context\MinkContext;
use Doctrine\ORM\EntityManager;
use Webmozart\Assert\Assert;

class FeatureContext extends MinkContext implements Context
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * FeatureContext constructor.
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @Given there are no feeds in the database
     */
    public function thereAreNoFeedsInTheDatabase()
    {
        $repo = $this->em->getRepository('AppBundle:Feed');

        $repo->createQueryBuilder('a')->delete()->getQuery()->execute();
    }


    /**
     * @Given I am on the add feed page
     */
    public function iAmOnTheAddFeedPage()
    {
        $this->visit('/feeds/add');
    }

    /**
     * @When I submit the feed
     */
    public function iSubmitTheFeed()
    {
        $page = $this->getSession()->getPage();

        $button = $page->findButton('Add');
        $button->click();
    }

    /**
     * @Then I should see :count feed columns
     */
    public function iShouldSeeFeedColumns($count)
    {
        $page = $this->getSession()->getPage();

        $feeds = $page->findAll('css', '.feed_content');

        Assert::count($feeds, $count);
    }

    /**
     * @Given There are :count feeds
     */
    public function thereAreFeeds($count)
    {
        for ($i = 1; $i <= $count; $i++) {
            $feed = new \AppBundle\Entity\Feed();
            $feed->setLink('http://www.php.net/news.rss');

            $this->em->persist($feed);
        }

        $this->em->flush();
    }

    /**
     * @When I view the homepage
     */
    public function iViewTheHomepage()
    {
        $this->visit('/');
    }

    /**
     * @When I edit the first feed
     */
    public function iEditTheFirstFeed()
    {
        $page = $this->getSession()->getPage();

        $link = $page->find('css', '.feed_header a');
        $link->click();
    }

    /**
     * @When I update the link to :link
     */
    public function iUpdateTheLinkTo($link)
    {
        $page = $this->getSession()->getPage();

        $page->fillField('feed_link', $link);
    }

    /**
     * @When I save the changes
     */
    public function iSaveTheChanges()
    {
        $page = $this->getSession()->getPage();

        $button = $page->findButton('Save');
        $button->click();
    }

    /**
     * @Then The first feed should be empty
     */
    public function theFirstFeedShouldBeEmpty()
    {
        $page = $this->getSession()->getPage();

        $feed = $page->find('css', '.feed_content');
        $items = $feed->findAll('xpath', '/*');

        Assert::count($items, 0);
    }

    /**
     * @When I view the single view feed page with a valid feed
     */
    public function iViewTheSingleViewFeedPageWithAValidFeed()
    {
        $this->visit('/view?feed=http%3A%2F%2Fwww.php.net%2Fnews.rss');
    }

    /**
     * @Then I should see some items
     */
    public function iShouldSeeSomeItems()
    {
        $page = $this->getSession()->getPage();

        $feed = $page->find('css', '.feed_content');
        $items = $feed->findAll('xpath', '/*');

        Assert::minCount($items, 1);
    }
}
